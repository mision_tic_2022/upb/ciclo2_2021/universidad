import java.util.ArrayList;

public class Facultad {
    /*************
     * Atributos
     *************/
    private String nombre;
    private String codigo;
    private Universidad universidad;
    private ArrayList<Materia> materias;

    public Facultad(String nombre, String codigo, Universidad universidad){
        this.nombre = nombre;
        this.codigo = codigo;
        this.universidad = universidad;
        this.materias = new ArrayList<Materia>();
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getCodigo() {
        return codigo;
    }
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void crear_materia(){
        Materia objMateria = new Materia("Fisica", "12345");
        this.materias.add(objMateria);
    }

}
